sap.ui.define([
    "sap/ui/core/Component",
    "sap/m/MessageToast",
    "be/fiddle/scanner/util/ScannerDetection"
], function (Component, Toast, Scanner) {
    "use strict";

    /**
    * @name be.fiddle.scanner.Component
    * @alias be.fiddle.scanner.Component
    * @author Tom Van Doorslaer
    * @constructor
    * @public
    * @extends sap.ui.core.Component
    * @class
    * <p>The scanner plugin is registered to the fiori launchpad. In Essence, it doesn't really need an icon, but we've added
    * anyway to visualize whether the scan detection is active or not.</p>
    * <p>Once activated, the plugin attaches itself to keyboard input, and tries to decide whether a certain inputstring
    * was typed so swiftly, that it's likely a barcode scanner. When such a scanned-string is detected, the plugin raises
    * an event.</p>
    **/
    var scanner = Component.extend("be.fiddle.scanner.Component", {
        metadata: {
            manifest: "json",
            properties:{
                /**
				 * internal instance of the ScannerDetection class. <br />
				 * 
				 * @public
				 * @instance
				 * @memberof be.fiddle.scanner.Component
				 */
                _scanner:{},
                
                /**
                 * What is the git URL of your project? Set this so that your the issuelogger plugin can direct you to the right 
                 * location for issues.
                 * @property {string} gitUrl
                 * @memberof be.fiddle.scanner.Component
                 */
                    "gitUrl":{
                    type:"string",
                    defaultValue:"https://gitlab.com/fiddlebe/ui5/plugins/scanner",
                    //mandatory:true,   //this attribute doesn't really exist, but I would like it to.
                    //readonly:true,    //this attribute doesn't really exist, but I would like it to.
                    byValue:"true"
                }
	        },
			events:{
                "scanDetected": {
                    /**
                     * <p>scanDetected  - When the scanner plugin detects a scanned barcode, the scanDetected method is launched
                     * </p><p>This event is launched both directly (on component) as globally (via the sap.ui.getCore().getEventBus()).
                     * This allows any application running in the launchpad to register a handler to the event.</p>
                     * <code>sap.ui.getCore().getEventBus().subscribe("be.fiddle.scanner", "scanDetected", oHandler, binding)</code>
                     * @event #scanDetected
                     * @type {object}
                     * @property {string} barcode - The barcode string
                     * @memberof be.fiddle.scanner.Component
                     */
                    parameters: [{
                            name: "barcode"
                        }
                    ]
                },
                /**
                 * Internal event. When a property on the component is changed (typically from the outside) , we
                 * notify any internal listeners (controllers) that a property was changed. This event typicall
                 * triggers a filtering of the internal dataset.
                 * 
                 * @event _propertyChanged
                 * @memberof be.fiddle.architect.Component
                 * @param {string} property - the name of the property that changed
                 * @param {any} value - the new value of the property
                 */
                "_propertyChanged":{ 
                    parameters:{
                        "property":"string",
                        "value":"any"
                    }
                }
            }
        },

        /**
        * <p>constructs the plugin. Nothing special here.</p>
        *
        * @constructs be.fiddle.scanner.Component
        * @public
        * @memberof be.fiddle.scanner.Component
        * @author Tom Van Doorslaer
        **/
        constructor: function(){
            Component.prototype.constructor.apply(this,arguments);
            this._scanner = new Scanner({
                onComplete:this.onScanDetected.bind(this),
                onError:this.onScanError.bind(this)
            });
            this._scanner.startScanning();
        },

        /**
        * <p>destroy the plugin. Nothing special here.</p>
        *
        * @method destroy
        * @public
        * @memberof be.fiddle.scanner.Component
        * @author Tom Van Doorslaer
        **/
        destroy: function(){
            Component.prototype.destroy.apply(this,arguments);
            if(this._scanner){
                this._scanner.stopScanning();
                this._scanner = null;
            }
        }
    });

    /**
    *
    * @method init
    * @public
    * @instance
    * @memberof be.fiddle.scanner.Component
    * @author Tom Van Doorslaer
    **/
    scanner.prototype.init = function () {
        this._headerItem = sap.ushell.Container.getRenderer("fiori2").addHeaderEndItem(
            {
                id: "barcodeIndicator",
                icon: "sap-icon://bar-code",
                //tooltip: resources.i18n.getText(""),
                text: this.getModel("i18n").getResourceBundle().getText("be.fiddle.scanner.title"),
                press: this.toggleScanner.bind(this)
            },
            true, 
            false,
            ["app","home"]
        );
    };

    /**
    *
    * @method toggleScanner
    * @public
    * @instance
    * @memberof be.fiddle.scanner.Component
    * @author Tom Van Doorslaer
    * when the plugin starts, it automatically launches the scandetection. by clicking it, you can toggle the scanstate
    **/
    scanner.prototype.toggleScanner = function(){
        if (this._scanner){
            this._scanner.isActive ? this.stopScanner() : this.startScanner()
        }
    };

    /**
    *
    * @method startScanner
    * @public
    * @instance
    * @memberof be.fiddle.scanner.Component
    * @author Tom Van Doorslaer
    * manually stop the scanner and overrule any internal state.
    **/
    scanner.prototype.startScanner = function(){
        if (this._headerItem){
        }

        if (this._scanner && !this._scanner.isActive){
            this._scanner.startScanning();
        }
    };

    /**
    *
    * @method stopScanner
    * @public
    * @instance
    * @memberof be.fiddle.scanner.Component
    * @author Tom Van Doorslaer
    * manually stop the scanner and overrule any internal state.
    **/
     scanner.prototype.stopScanner = function(){
        if (this._headerItem){
        }

        if (this._scanner && this._scanner.isActive){
            this._scanner.stopScanning();
        }
    };
    
    /**
    *
    * @method onScanDetected
    * @public
    * @instance
    * @memberof be.fiddle.scanner.Component
    * @author Tom Van Doorslaer
    * a scan has been detected. raise an event.
    **/
    scanner.prototype.onScanDetected = function(barcode){
        Toast.show( this.getModel("i18n").getResourceBundle().getText("be.fiddle.scanner.scanDetected", [barcode]) );
        //launch event global, so any application can listen to it.
        sap.ui.getCore().getEventBus().publish("be.fiddle.scanner","scanDetected", {barcode:barcode } ) 
        //but also launch it locally for any direct attached handlers.
        this.fireEvent( "scanDetected", {barcode:barcode });
    };
    
    /**
    *
    * @method onScanError
    * @public
    * @instance
    * @memberof be.fiddle.scanner.Component
    * @author Tom Van Doorslaer
    * a scan has been detected. raise an event.
    **/
     scanner.prototype.onScanError = function(event){
         Toast.show( this.getModel("i18n").getResourceBundle().getText("be.fiddle.scanner.error") );
    };

    return scanner;
});