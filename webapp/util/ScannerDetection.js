sap.ui.define(
  [
	  "sap/ui/base/Object"
  ], 
  function(Object) {
    "use strict";

    /**
    * @name be.fiddle.scanner.util.ScannerDetection
    * @alias be.fiddle.scanner.util.ScannerDetection
    * @author Tom Van Doorslaer
    * @constructor
    * @public
    * @extends sap.ui.base.Object
    * @class
    based on https://github.com/asaf050/js-scanner-detection/blob/master/src/lib/index.js, but adapted for UI5 compatibility
    */
    return Object.extend("be.fiddle.scanner.util.ScannerDetection", {

      constructor:function (options) {
        if (!options) {
          options = {};
        }

        this.options = {
          onComplete: options.onComplete || false, // Callback after detection of a successful scanning
          onError : options.onError || false, // Callback after detection of a unsuccessful scanning
          onReceive : options.onReceive || false, // Callback after receive a char
          timeBeforeScanTest : options.timeBeforeScanTest ||  100, // Wait duration (ms) after keypress event to check if scanning is finished
          avgTimeByChar : options.avgTimeByChar ||  30, // Average time (ms) between 2 chars. Used to do difference between keyboard typing and scanning
          minLength : options.minLength ||  6, // Minimum length for a scanning
          endChar : options.endChar ||  [9, 13], // Chars to remove and means end of scanning
          stopPropagation : options.stopPropagation ||  false, // Stop immediate propagation on keypress event
          preventDefault : options.preventDefault ||  false // Prevent default action on keypress event  
        };

        // Callback after detection of a successful scanning
        this.firstCharTime = 0;
        this.lastCharTime = 0;
        this.stringWriting = '';
        this.callIsScanner = false;
        this.testTimer = false;
        this.isActive = false;
        this.startScanning();
      },

      keypress:function (e) {
        if (this.options.stopPropagation) e.stopImmediatePropagation()
        if (this.options.preventDefault) e.preventDefault()

        if (this.firstCharTime && this.options.endChar.indexOf(e.which) !== -1) {
          e.preventDefault()
          e.stopImmediatePropagation()
          this.callIsScanner = true
        } else {
          this.stringWriting += String.fromCharCode(e.which)
          this.callIsScanner = false
        }

        if (!this.firstCharTime) {
          this.firstCharTime = e.timeStamp
        }
        this.lastCharTime = e.timeStamp

        if (this.testTimer) clearTimeout(this.testTimer)
        if (this.callIsScanner) {
          this.scannerDetectionTest()
          this.testTimer = false
        } else {
          this.testTimer = setTimeout(() => {
            this.scannerDetectionTest()
          }, this.options.timeBeforeScanTest)
        }

        if (this.options.onReceive) this.options.onReceive.call(this, e)
        this.trigger('scannerDetectionReceive', { evt: e })
      },
    
      trigger:function (options) {},

      scannerDetectionTest:function (s) {
        // If string is given, test it
        if (s) {
          this.firstCharTime = this.lastCharTime = 0
          this.stringWriting = s
        }
        // If all condition are good (length, time...), call the callback and re-initialize the plugin for next scanning
        // Else, just re-initialize
        if (
          this.stringWriting.length >= this.options.minLength &&
          this.lastCharTime - this.firstCharTime <
            this.stringWriting.length * this.options.avgTimeByChar
        ) {
          if (this.options.onComplete) {
            this.options.onComplete.call(this, this.stringWriting)
          }
          this.initScannerDetection()
          return true
        } else {
          if (this.options.onError) {
            this.options.onError.call(this, this.stringWriting)
          }
          this.initScannerDetection()
          return false
        }
      },

      initScannerDetection:function () {
        this.firstCharTime = 0
        this.stringWriting = ''
      },
      
      startScanning:function () {
        this.isActive = true;
        document.onkeydown = this.keypress.bind(this);
      },

      stopScanning:function () {
        document.onkeydown = null;
        this.isActive = false;
      }
    });
  }
);